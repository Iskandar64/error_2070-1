package mx.itesm.error_2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Daniel Alillo on 12/11/2017.
 */

public class PantallaHistoria extends ScreenAdapter{

    private final Error game;
    private static final float WIDTH = 1280;
    private static final float HEIGHT = 720;
    private Stage stage;

    private Texture sig;//seguir con la historia
    private Texture sigPress;//cuando presionemos atras

    private int fNumber = 0;
    private int fLimit;

    private int level;

    private final AssetManager manager;

    private Pantallas siguientePantalla;


    private ImageButton atBtn;
    private Image [] backgrounds;

    protected SpriteBatch batch;

    public PantallaHistoria(Error game, Pantallas siguientePantalla){
        this.game = game;
        this.siguientePantalla = siguientePantalla;
        manager = game.getAssetManager();
    }

    @Override
    public void show(){

        super.show();
        stage = new Stage(new FillViewport(WIDTH, HEIGHT));

        Gdx.input.setInputProcessor(stage);

        backgrounds = new Image[13];

        switch (siguientePantalla){
            case NIVEL_RUNNER:
                backgrounds[0] = new Image((Texture)manager.get("Fondos/Nivel1/nivel1History1.jpg"));
                backgrounds[1] = new Image((Texture)manager.get("Fondos/Nivel1/nivel1History2.jpg"));
                backgrounds[2] = new Image((Texture)manager.get("Fondos/Nivel1/nivel1History3.jpg"));
                backgrounds[3] = new Image((Texture)manager.get("Fondos/Nivel1/nivel1History4.jpg"));

                stage.addActor(backgrounds[3]);
                stage.addActor(backgrounds[2]);
                stage.addActor(backgrounds[1]);
                stage.addActor(backgrounds[0]);

                fLimit =3;
                level = 1;
                break;

            case NIVEL_COLLECT:
                backgrounds[0] = new Image((Texture)manager.get("Fondos/Nivel2/nivel2History1.jpg"));
                backgrounds[1] = new Image((Texture)manager.get("Fondos/Nivel2/nivel2History2.jpg"));
                backgrounds[2] = new Image((Texture)manager.get("Fondos/Nivel2/nivel2History3.jpg"));


                stage.addActor(backgrounds[2]);
                stage.addActor(backgrounds[1]);
                stage.addActor(backgrounds[0]);
                fLimit =2;
                level = 2;
                break;

            case WINNER2:
                backgrounds[0] = new Image((Texture)manager.get("Fondos/Nivel2/nivel2Final1.jpg"));
                backgrounds[1] = new Image((Texture)manager.get("Fondos/Nivel2/nivel2Final2.jpg"));

                stage.addActor(backgrounds[1]);
                stage.addActor(backgrounds[0]);
                fLimit =1;
                level = 10;

            case NIVEL_DOOM:
                backgrounds[0] = new Image((Texture)manager.get("Fondos/Nivel3/final1.jpg"));
                backgrounds[1] = new Image((Texture)manager.get("Fondos/Nivel3/final2.jpg"));
                backgrounds[2] = new Image((Texture)manager.get("Fondos/Nivel3/final3.jpg"));
                backgrounds[3] = new Image((Texture)manager.get("Fondos/Nivel3/parte1.jpg"));
                backgrounds[4] = new Image((Texture)manager.get("Fondos/Nivel3/parte2.jpg"));
                backgrounds[5] = new Image((Texture)manager.get("Fondos/Nivel3/parte3.jpg"));
                backgrounds[6] = new Image((Texture)manager.get("Fondos/Nivel3/parte4.jpg"));
                backgrounds[7] = new Image((Texture)manager.get("Fondos/Nivel3/parte5.jpg"));
                backgrounds[8] = new Image((Texture)manager.get("Fondos/Nivel3/parte6.jpg"));
                backgrounds[9] = new Image((Texture)manager.get("Fondos/Nivel3/parte7.jpg"));
                backgrounds[10] = new Image((Texture)manager.get("Fondos/Nivel3/parte8.jpg"));
                backgrounds[11] = new Image((Texture)manager.get("Fondos/Nivel3/parte9.jpg"));
                backgrounds[12] = new Image((Texture)manager.get("Fondos/Nivel3/parte10.jpg"));

                stage.addActor(backgrounds[12]);
                stage.addActor(backgrounds[11]);
                stage.addActor(backgrounds[10]);
                stage.addActor(backgrounds[9]);
                stage.addActor(backgrounds[8]);
                stage.addActor(backgrounds[7]);
                stage.addActor(backgrounds[6]);
                stage.addActor(backgrounds[5]);
                stage.addActor(backgrounds[4]);
                stage.addActor(backgrounds[3]);
                stage.addActor(backgrounds[2]);
                stage.addActor(backgrounds[1]);
                stage.addActor(backgrounds[0]);

                fLimit = 12;
                level = 3;
                break;
        }

        sig = new Texture(Gdx.files.internal("botones/siguiente.png"));
        sigPress =  new Texture(Gdx.files.internal("botones/siguiente1.png"));

        atBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(sig)),
                new TextureRegionDrawable((new TextureRegion(sigPress))));
        atBtn.setPosition(800,25);
        stage.addActor(atBtn);
        atBtn.addListener(new ActorGestureListener(){
            public void tap(InputEvent event, float x, float y, int count, int button){
                super.tap( event,  x,  y,  count, button);
                if(fNumber<fLimit){
                    borrarScreen();
                }else if(level<10){
                    game.setScreen(new PantallaRunner(game, level));
                    dispose();
                }else{
                    game.setScreen(new PantallaGanador(game));
                    dispose();
                }
                fNumber++;
            }
        });
    }

    @Override
    public void render(float delta){

        super.render(delta);
        clearScreen();

        stage.act(delta);
        stage.draw();

    }

    public void borrarScreen(){
        stage.clear();

        for(int i= fLimit;i>fNumber; i-- ){
            stage.addActor(backgrounds[i]);
        }

        stage.addActor(atBtn);
    }

    @Override
    public void resize(int width, int height){

        super.resize(width, height);

        stage.getViewport().update(width, height);

    }


    private void clearScreen(){
        Gdx.gl.glClearColor((float)0, (float)0, 0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public void dispose(){
        super.dispose();;
        stage.dispose();
        sig.dispose();
        sigPress.dispose();
    }

}
