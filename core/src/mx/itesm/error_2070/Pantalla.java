package mx.itesm.error_2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Esta clase representa cualquier pantalla que se quiera mostrar
 *
 * Contiene una cámara y la vista
 */

public abstract class Pantalla implements Screen{

    public static final float ANCHO = 1280;
    public static final float ALTO = 720;

    // Disponibles en las subclases
    // Cámara, vista y objeto batch para dibujar
    protected OrthographicCamera camara;
    protected Viewport vista;
    protected SpriteBatch batch;
    protected Preferences pref;

    // Constructor. Crea la cámara, vista y batch.
    public Pantalla(){
        pref = Gdx.app.getPreferences("My Preferences");
        camara = new OrthographicCamera(ANCHO, ALTO);
        camara.position.set(ANCHO/2, ALTO/2, 0);
        camara.update();
        vista = new StretchViewport(ANCHO, ALTO, camara);
        batch = new SpriteBatch();
    }

    // Borra en negro
    protected void borrarPantalla(){
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    protected void borrarPantalla(float r, float g, float b){
        Gdx.gl.glClearColor(r,g,b,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    protected void actualizarVista(){}

    @Override
    public void resize(int width, int height){
        vista.update(width, height);
    }

    @Override
    public void hide(){dispose();}

}
